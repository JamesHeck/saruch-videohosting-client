# Saruch-VideoHosting-Client
client to manage your videos in Saruch.co


* copy videos/folders
* move videos/folders
* rename videos/folders
* delete videos/folders
* upload download videos with a realtime progress tracking
* share videos
* public/private videos
* monetize/unmonetize video
* Grab stream link in multiable video resolution (360-480-720-1080)
* upload videos folder
* upload zipped video
* drag-drop upload
* remote upload (direct url upload)
* remote upload status monitor
* search videos in your account
* set/delete video thumbnail
* grab video detailed information
* multi videos upload

`Download:`
[https://github.com/jamesheck2019/Saruch-VideoHosting-Client/releases](https://github.com/jamesheck2019/Saruch-VideoHosting-Client)

![https://i.postimg.cc/P5CBx1cc/Saruch-Video-Hosting-Client-LWp-REN9-I5-R.png](https://i.postimg.cc/P5CBx1cc/Saruch-Video-Hosting-Client-LWp-REN9-I5-R.png)
